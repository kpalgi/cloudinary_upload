<html>
<head>
    <meta charset="utf-8">
    <title>Pildi üles laadimine Cloudinary.com</title>
    <link rel="shortcut icon"
          href="http://cloudinary.com/favicon.png"/>
    <style>
        body {
            font-family: Helvetica, Arial, sans-serif;
            color: #333;
        }

        .item {
            margin: 20px;
            width: 600px;
            padding-bottom: 20px;
            border-bottom: 1px solid #ccc
        }

        .caption {
            margin-bottom: 10px;
            font-weight: bold;
            color: #0b63b6;
        }

        .public_id {
            margin-top: 5px;
            font-size: 12px;
            color: #666;
        }

        h1 {
            color: #0e2953;
        }

        h2, h3 {
            color: #0e5e01;
        }

        .link {
            margin-top: 5px;
        }

        .link a {
            font-size: 12px;
            color: #666;
        }
    </style>
</head>
<body>
    <h1>Pildi üles laadimine Cloudinary.com ja pildi allkirja lisamine</h1>
<?php
	$text_position = $_POST['text_position'];
	$text_size = $_POST['text_size'];
	$url = $_POST['url'];
	$old_text_size = $_POST['old_text_size'];
	$old_text_pos = $_POST['old_text_pos'];
	$txt_align = $_POST['txt_align'];
	$text = $_POST['text'];
	
	$exploder1 = 'Arial_' . $old_text_size;
	$brake_url1 = explode($exploder1, $url);
	$exploder2 = 'g_south,y_' . $old_text_pos;
	$brake_url2 = explode($exploder2, $brake_url1[1]);
	
	$new_url = $brake_url1[0] . 'Arial_' . $text_size . $brake_url2[0] . 'g_south,y_' . $text_position . $brake_url2[1];
	
	echo '<img src="'. $new_url .'" align="right" />';
	
	echo 'URL: <input size=\'55\' id=\'myInput2\' type=\'text\' name=\'copy2\' value=\'' . $new_url . '\'><button onclick="myFunction2()">Kopeeri link</button><br><br>';
	echo 'HTML kood: <input size=\'55\' id=\'myInput\' type=\'text\' name=\'copy\' value=\'<img src="' . $new_url . '" align="'.$txt_align.'" alt="'.$text.'" />\'>';
	echo '<button onclick="myFunction()">Kopeeri kood</button><br><br>';
?>
<button onclick="goBack()"><< Tagasi muutma</button><br><br>
<form action="http://projects.crewnew.com/cloudinary/cloudinary">
    <input type="submit" value="Lae uus pilt" />
</form>
<form action="https://cloudinary.com/console/media_library/folders/all/">
    <input type="submit" value="Halda pilte Cloudinary's" />
</form>
<br>
Kasutaja: eks@kirjandus.ee / Parool: Asdc1523!
<script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  document.execCommand("copy");
}

function myFunction2() {
  var copyText = document.getElementById("myInput2");
  copyText.select();
  document.execCommand("copy");
}

function goBack() {
    window.history.back();
}
</script>
</body>
</html>
	