<?php
// text position and size after upload
// instructions

require 'src/Cloudinary.php';
require 'src/Uploader.php';
require 'src/Helpers.php';
require 'src/Api.php';

$path_to = '/home/estoniac/public_html/crewnew.com/projects/cloudinary/cloudinaryuploads/';

\Cloudinary::config(array(
    'cloud_name' => 'eks',
    'api_key' => '921312356192551',
    'api_secret' => '57Q76vA4AYVCLuzfVteIMAOnF4g'
));

$currentDir = getcwd();
$uploadDirectory = "uploads/";
$text = $_POST['txt'];
$width = $_POST['width'];

$errors = []; // Store all foreseen and unforseen errors here

$fileExtensions = ['jpeg','jpg','png', 'gif']; // Get all the file extensions

$fileName = $_FILES['myfile']['name'];
$fileSize = $_FILES['myfile']['size'];
$fileTmpName  = $_FILES['myfile']['tmp_name'];
$fileType = $_FILES['myfile']['type'];
$fileExtension = strtolower(end(explode('.',$fileName)));

$uploadPath = $currentDir . $uploadDirectory . basename($fileName); 

$file_name4 = basename($fileName);

    if (isset($_POST['submit'])) {

        if (! in_array($fileExtension,$fileExtensions)) {
            $errors[] = "This file extension is not allowed. Please upload a JPEG, JPG, GIF or PNG file";
        }

        if ($fileSize > 4000000) {
            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
        }

        if (empty($errors)) {
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

            if ($didUpload) {
                //echo "The file " . basename($fileName) . " has been uploaded";
            } else {
                echo "An error occurred somewhere. Try again or contact the admin";
            }
        } else {
            foreach ($errors as $error) {
                echo $error . "These are the errors" . "\n";
            }
        }
    }

$sample_paths = array(
    'pizza' => $path_to . basename($fileName),
    'lake' => getcwd() . DIRECTORY_SEPARATOR . 'lake.jpg',
);

$default_upload_options = array('tags' => 'crewnew_uploader');
$eager_params = array('width' => 200, 'height' => 150, 'crop' => 'scale');
$files = array();

function do_uploads($file_name5)
{
    global $files, $sample_paths, $default_upload_options, $eager_params;

    # public_id will be generated on Cloudinary's backend.
    //$files['unnamed_local'] = \Cloudinary\Uploader::upload($sample_paths['pizza'], $default_upload_options);

    # Same image, uploaded with a public_id
    $files['named_local'] = \Cloudinary\Uploader::upload(
        $sample_paths['pizza'],
        array_merge(
            $default_upload_options,
            array(
                'public_id' => $file_name5,
                'eager' => $eager_params,
            )
        )
    );
}

/**
 * Output an image in HTML along with provided caption and public_id
 *
 * @param        $img
 * @param array  $options
 * @param string $caption
 */
function show_image($img, $options = array(), $caption = '', $width)
{
    echo '<h3>Pildi URL ja kood:</h3>';
	$options['format'] = $img['format'];
    $transformation_url = cloudinary_url($img['public_id'], $options);

	$get_url = explode('upload/', $transformation_url);
	$text = explode('.', $get_url[1]);
	$pattern = 'c_scale,w_' . $width . '/';
	$text = str_replace($pattern,'', $text[0]);
	$text_size = $_POST['text_size'];
	$text_position = $_POST['text_position'];
	$new_url = $get_url[0] . 'upload/l_text:Arial_' . $text_size . ':' . $text . ',g_south,y_' . $text_position . '/' . $get_url[1];
	
    echo '<img src="'. $new_url .'" align="right" /><div class="link">URL: <input size=\'55\' id=\'myInput2\' type=\'text\' name=\'copy2\' value=\'' . $new_url . '\'><button onclick="myFunction2()">Kopeeri link</button><br><br></div>';
    echo '</div>';
	return $new_url;
}

?>
<html>
<head>
    <meta charset="utf-8">
    <title>Pildi üles laadimine Cloudinary.com</title>
    <link rel="shortcut icon"
          href="<?php echo cloudinary_url('http://cloudinary.com/favicon.png', array('type' => 'fetch')); ?>"/>
    <style>
        body {
            font-family: Helvetica, Arial, sans-serif;
            color: #333;
        }

        .item {
            margin: 20px;
            width: 600px;
            padding-bottom: 20px;
            border-bottom: 1px solid #ccc
        }

        .caption {
            margin-bottom: 10px;
            font-weight: bold;
            color: #0b63b6;
        }

        .public_id {
            margin-top: 5px;
            font-size: 12px;
            color: #666;
        }

        h1 {
            color: #0e2953;
        }

        h2, h3 {
            color: #0e5e01;
        }

        .link {
            margin-top: 5px;
        }

        .link a {
            font-size: 12px;
            color: #666;
        }
    </style>
</head>
<body>
    <h1>Pildi üles laadimine Cloudinary.com ja pildi allkirja lisamine</h1>
    <?php if (!isset($_POST['submit'])): ?>
	<form action="index.php" method="post" enctype="multipart/form-data">
        Pildi allkiri: <input type="text" name="txt" size="30" /><br><br>
		Pildi positsioon: <select name="txt_align"><option value="right">Paremal</option><option value="left">Vasakul</option></select><br><br>
		Pildi laius pikslites: <input type="text" name="width" value="300" size="3" />(ei soovita muuta)
		<br /><br />
		Teksti suurus: <input type="text" name="text_size" value="30" size="3" />(ei soovita muuta)
		<br /><br />
		Teksti kaugus pildi all: <input type="text" name="text_position" value="-40" size="3" />(ei soovita muuta aga näiteks -20 on pildile lähemal)
		<br /><br />
		Vali fail:
        <input type="file" name="myfile" id="fileToUpload">
        <input type="submit" name="submit" value="Lae üles" >
    </form>
	<?php endif; ?>
<?php
if (isset($_POST['submit'])) { 
	do_uploads($text); 
    $link_to_image = show_image(
        $files['named_local'],
        array('width' => $width, 'crop' => 'scale'),
        $file_name4, $width
    );
	$txt_align = $_POST['txt_align'];
	echo 'HTML kood: <input size=\'55\' id=\'myInput\' type=\'text\' name=\'copy\' value=\'<img src="' . $link_to_image . '" align="'.$txt_align.'" alt="'.$text.'" />\'>';
	echo ' <button onclick="myFunction()">Kopeeri kood</button>';
	echo '<h4>Muuda teksti suurust ja positsiooni:</h4>';
	$text_position = $_POST['text_position'];
	$text_size = $_POST['text_size'];
	echo '<form action="edit.php" method="post">Teksti suurus: <input type="text" name="text_size" value="'.$text_size.'" size="3" /><br /><br />Teksti kaugus pildi all: <input type="text" name="text_position" value="'.$text_position.'" size="3" /><input type="hidden" name="url" value="'.$link_to_image.'"><input type="hidden" name="old_text_size" value="'.$text_size.'"><input type="hidden" name="old_text_pos" value="'.$text_position.'"><input type="hidden" name="text" value="'.$text.'"><input type="hidden" name="txt_align" value="'.$txt_align.'"><br><br><input type="submit" value="Muuda"></form>';
	echo '<form action="http://projects.crewnew.com/cloudinary/cloudinary"><input type="submit" value="Lae uus pilt" /></form>';
	echo '<form action="https://cloudinary.com/console/media_library/folders/all/"><input type="submit" value="Halda pilte Cloudinary\'s" /></form>';
	echo '<br>Kasutaja: eks@kirjandus.ee / Parool: Asdc1523!';
}
$full_path = $path_to . $file_name4;
unlink($full_path);

?>
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
<h3>Õpetus</h3>
<p><a href="https://youtu.be/J1CJOZQGDik" target="_blank">Vaata video õpetust>></a></p>
<ol>
<li>Kirjuta pildi allkiri</li>
<li>Lae arvutist soovitatavalt mitte suurem kui 2MB pildifail (JPEG, JPG, PNG või GIF)</li>
<li>Kui tekst on liiga väike siis muuda suurust 20ks näiteks või kui liiga väike siis 40ks jne. Kui tekst võis olla üleval pool, proovi panna teksti kauguseks näiteks -20, kui võiks rohkem allpool olla siis proovi -60 jne.</li>
<li>Kui ikka tundub, et võiks parem olla, siis klikka "Tagasi muutma" nuppu ja proovi numbreid muuta.</li>
<li>Lisa pilt veebi:
<ul>
	<li>Võimalus 1: Klikka "Kopeeri link" nuppu ja killa sisuhalduses nupureal "Insert/edit image" nuppu või vali rippmenüüst "Insert" sama asi. Ja "Source" väljale kleebi pildi URL.</li>
	<li>Võimalus 2: <font size="2">(See võimaldab ka pildi asukohta paremal/vasakul määrata ja tekst saab olema pildi ümber.)</font> Klikka "Kopeeri kood" nuppu ja sisuhalduses nupureal klikka all paremal viimast nuppu "Source code" ning kleebi HTML kood ükskõik kuhu teksti sisse.</li>
</ul>
</li>
</ol>
<p>Siin <a href="https://cloudinary.com/console/media_library" target="_blank">https://cloudinary.com/console/media_library</a> kasutaja: eks@kirjandus.ee / parool: Asdc1523! saab mugavalt pilte hallata ja töödelda. Cloudinary on tänapäeval nr. 1 koht piltide haldamiseks/manageerimiseks tarkvara siseselt.</p>
<script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  document.execCommand("copy");
}
function myFunction2() {
  var copyText = document.getElementById("myInput2");
  copyText.select();
  document.execCommand("copy");
}
</script>
</body>
</html>